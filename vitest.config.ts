import { defineConfig } from 'vitest/config';


export default defineConfig({
	test: {
		coverage: {
			enabled: true,
			all: true,
			reporter: [ 'html-spa', 'text-summary' ],
			include: [
				'packages/common/src/**/*',
				'packages/feed/src/**/*',
				// 'packages/srv/src/**/*',
				// 'packages/www/src/**/*',
			],
			reportsDirectory: './.coverage',
			reportOnFailure: true,
		},
		outputFile: {
			junit: 'test-results.xml',
		},
	},
});
