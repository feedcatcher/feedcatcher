const path = require('node:path');
const fs = require('fs-extra');
const { ROOT_DIR, SRV_DIR, WWW_DIR } = require('./_util');

const outDir = path.join(ROOT_DIR, 'dist');
const files = {
	[path.join(SRV_DIR, 'dist', 'main.js')]: 'main.js',
	[path.join(SRV_DIR, 'dist', 'main.js.map')]: 'main.js.map',
	[path.join(WWW_DIR, 'dist')]: 'static',
}

fs.removeSync(outDir);
fs.mkdirSync(outDir, { recursive: true });

Promise.all(
	Object.entries(files).map(([ src, dest ]) => {
		const fullDest = path.join(outDir, dest);
		console.log('Copying', src, '->', dest);
		return fs.copy(src, fullDest);
	})
).then(() => (
		fs.remove(path.join(outDir, 'static', 'tsconfig.tsbuildinfo'))
	))


