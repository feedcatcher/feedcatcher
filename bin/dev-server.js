const path = require('node:path');
const fs = require('node:fs');
const { setTimeout } = require('node:timers/promises')
const { spawn } = require('node:child_process');
const { SRV_DIR } = require('./_util');


const MAIN = path.join(SRV_DIR, 'dist', 'main.js');
const TIMEOUT = 100;

const waitForMain = async (attempt=0) => {
	if(fs.existsSync(MAIN)) {
		return;
	}

	if(attempt > 100) {
		throw new Error(`Failed to find ${MAIN} after ${100 & TIMEOUT}ms.`);
	}

	await setTimeout(TIMEOUT);
	return waitForMain(attempt + 1);
}

waitForMain().then(() => {
	console.log('Starting server...');
	spawn('node', [ '--watch', MAIN ], {
		stdio: 'inherit',
		cwd: SRV_DIR,
		env: {
			NODE_ENV: 'development',
			...process.env,
		}
	});
})


