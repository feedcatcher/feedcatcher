import { describe, expect, it } from 'vitest';
import { common } from '../src';


describe('[common] test', () => {
	it('exports the word common', () => {
		expect(common)
			.to.eq('common!');
	});
});
