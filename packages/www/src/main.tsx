import type { JSX } from 'preact/jsx-runtime';
import { render } from 'preact';
import { useEffect, useState } from 'preact/hooks';


const Root = (): JSX.Element => {
	const [ message, setMessage ] = useState<string | null>(null);

	useEffect(() => {
		fetch('/api')
			.then(r => r.text())
			.then(setMessage);
	}, []);

	return (
		<>
			<h1>feedcatcher</h1>

			{ message && <p>API says: <code>{ message }</code></p> }
		</>
	);
};

window.addEventListener('load', () => {
	const root = document.getElementById('root')!;

	render(<Root/>, root);
});
