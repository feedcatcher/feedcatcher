import { describe, expect, it } from 'vitest';
import { feed } from '../src';


describe('[feed] test', () => {
	it('exports the word feed', () => {
		expect(feed)
			.to.eq('feed!');
	});
});
