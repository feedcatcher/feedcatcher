import 'source-map-support/register';

import fastify from 'fastify';


fastify()
	.get('/api', () => 'hello, world')
	.listen({ port: 8080 })
	.then(() => console.log('Server started')); // eslint-disable-line no-console

